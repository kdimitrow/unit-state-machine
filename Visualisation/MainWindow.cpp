#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "ParserData.h"
#include <QString>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ParserData data("statemachine.smr");

    stateMachine.Deserialize(data);
    ui->setupUi(this);
    connect(&timer, SIGNAL(timeout()), this, SLOT(Update()));
    connect(&eventTimer, SIGNAL(timeout()), this, SLOT(PushEvents()));
    timer.start(100);
    eventTimer.start(500);
    elapsedTime.start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::Update()
{
    stateMachine.Process();
    ui->currentStateView->setText(stateMachine.GetCurrentStateName());
    ui->counter->display(elapsedTime.elapsed());
}

void MainWindow::PushEvents()
{
    StateEvent event;
    event.eventType = StateEvent::kNextState;
    event.eventData = nullptr;
    stateMachine.PushEvent(event);
}
