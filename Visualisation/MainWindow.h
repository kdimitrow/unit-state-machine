#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "StateMachine.h"
#include <QTimer>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void Update();
    void PushEvents();

private:
    Ui::MainWindow *ui;
    StateMachine stateMachine;
    QTimer timer;
    QTimer eventTimer;
    QTime elapsedTime;
};

#endif // MAINWINDOW_H
