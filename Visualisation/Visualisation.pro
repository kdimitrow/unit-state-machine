#-------------------------------------------------
#
# Project created by QtCreator 2013-05-16T14:46:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Visualisation
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp

HEADERS  += MainWindow.h

FORMS    += MainWindow.ui

INCLUDEPATH += ../StateMachine
LIBS += -L../StateMachine -lStateMachine
