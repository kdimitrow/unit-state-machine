#ifndef STATECONDITIONS_H
#define STATECONDITIONS_H

class StateEvent;
class StateCondition;

typedef bool (*ConditionProc)(StateEvent *event,StateCondition *condition);

class StateCondition
{
public:
    StateCondition();
    ~StateCondition();
public:
ConditionProc condition;

static bool ConditionAlways(StateEvent *event,StateCondition *condition);
};



#endif // STATECONDITIONS_H
