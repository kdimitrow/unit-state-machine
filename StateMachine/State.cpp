#include "State.h"
#include<iostream>

State::State()
{
    enter = &State::DummyEnter;
    process = &State::DummyProcess;
    exit = &State::DummyExit;
}

State::~State()
{

}

void State::Enter()
{
}

State* State::Process()
{
    RegisteredEventsIterator regIt;

    while(!events.isEmpty())
    {
        StateEvent event = events.dequeue();

        regIt = registeredEvents.find(event.eventType);

        if(regIt != registeredEvents.end())
        {
            Conditions &conditions = regIt.value().conditions;
            bool conditionsResult = true;

            for(ConditionIterator it = conditions.begin();it != conditions.end(); ++it)
            {
                conditionsResult = conditionsResult && (*it).condition(&event,&(*it));
            }

            if(conditionsResult)
            {
                return (regIt.value().nextState);
            }
        }
    }

    if(process(this))
    {
        regIt = registeredEvents.find(StateEvent::kStateExpire);

#ifdef DEBUG
        if(regIt != registeredEvents.end())
        {
            return (regIt.value().nextState);
        }

        else
        {
            // Some error message
            return (nullptr);
        }

#else
     return (regIt.value().nextState);
#endif


    }

    return (nullptr);
}

void State::Exit()
{
}

void State::DummyEnter(State* thisState)
{
    UNUSED(thisState);
}

bool State::DummyProcess(State* thisState)
{
    UNUSED(thisState);

    return (false);
}

void State::DummyExit(State* thisState)
{
    UNUSED(thisState);
}

