#ifndef STATEEVENT_H
#define STATEEVENT_H

class StateEvent
{
public:
    enum EventType
    {
        kStateExpire,
        kNextState
    };

public:
    StateEvent();

public:
    EventType eventType;
    void *eventData;
};

#endif // STATEEVENT_H
