#ifndef UNITPARSER_H
#define UNITPARSER_H

#include "ParserData.h"
#include <QMap>
#include <QList>

typedef QString StateName;
typedef QString EventName;
typedef QString ConditionName;

typedef QMap<StateName,QList<EventName> > StatesContainer;


enum ReadingState
{
    kState,
    kEvent,
    kCondition,
    kLink,
    kComment,
    kParseError,
    kPass
};

class StateParser
{
public:
    StateParser();
    ~StateParser();

    bool Parse(const ParserData& data);
    void Serialize();

private:
    ReadingState WhatToRead(int positionInBuffer);
    int ReadState(int positionInBuffer);
    int ReadEvent(int positionInBuffer);
    int ReadCondition(int positionInBuffer);
    int ReadLink(int positionInBuffer);
    int ReadComment(int positionInBuffer);


private:
    ReadingState oldState;
    ReadingState currentState;
    const char* buffer;
    int size;



};

#endif // UNITPARSER_H
