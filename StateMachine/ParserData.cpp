#include "ParserData.h"
#include <fstream>

ParserData::ParserData(const char* data)
{
buffer = 0;//nullptr;
size = 0;

    if(data)
    {
        Init(data);
    }
}

ParserData::~ParserData()
{
    Clear();
}

void ParserData::Init(const char* data)
{
    Clear();

    std::ifstream input;
    input.open("statemachine.smr");
    input.seekg(0,std::ios::end);
    size = input.tellg();
    input.seekg(0,std::ios::beg);
    buffer = new char[size];
    input.read(buffer,size);
    input.close();
}

void ParserData::Clear()
{
    if(buffer)
    {
        delete buffer;
        buffer = 0;//nullptr;
        size = 0;
    }
}

const char* ParserData::GetRawData() const
{
    return (buffer);
}

long long ParserData::Size() const
{
    return (size);
}

