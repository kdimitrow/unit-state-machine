#include "StateConditions.h"
#include "StateEvent.h"


StateCondition::StateCondition()
{
    condition = &StateCondition::ConditionAlways;
}

StateCondition::~StateCondition()
{
}

bool StateCondition::ConditionAlways(StateEvent *event, StateCondition *condition)
{
    return (true);
}
