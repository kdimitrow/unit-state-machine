#ifndef EVENTCOMPLEMENT_H
#define EVENTCOMPLEMENT_H

#include "StateConditions.h"
#include <QList>

class State;
typedef QList<StateCondition> Conditions;
typedef QList<StateCondition>::iterator ConditionIterator;

class EventComplement
{
public:
    State *nextState;
    Conditions conditions;
};

#endif // EVENTCOMPLEMENT_H
