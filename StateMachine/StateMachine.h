#ifndef STATEMACHINE_H
#define STATEMACHINE_H
#include "State.h"
#include "ParserData.h"

class StateMachine
{
public:
    StateMachine();
    ~StateMachine();

    bool Deserialize(const ParserData& data);

    void Process();
    void PushEvent(StateEvent event);

    const QString &GetCurrentStateName() const;

private:
    State *currentState;
};

#endif // STATEMACHINE_H
