#include "StateMachine.h"
#include "StateParser.h"
#include <iostream>

StateMachine::StateMachine()
{
}

StateMachine::~StateMachine()
{
    delete currentState;
}

bool StateMachine::Deserialize(const ParserData &data)
{
    StateParser parser;

    if(!parser.Parse(data))
    {
        abort();
    }

    currentState = new State();
    currentState->stateName = "Idle state";
    State * anotherState = new State();
    anotherState->stateName = "Move state";
    EventComplement eventComplement1;
    EventComplement eventComplement2;
    eventComplement1.nextState = anotherState;
    eventComplement1.conditions.append(StateCondition());
    eventComplement2.nextState = currentState;
    eventComplement2.conditions.append(StateCondition());
    currentState->registeredEvents.insert(StateEvent::kNextState,eventComplement1);
    anotherState->registeredEvents.insert(StateEvent::kNextState,eventComplement2);

    return (true);
}

void StateMachine::Process()
{
    State *newState = nullptr;

    if(currentState)
        newState = currentState->Process();

    if(newState)
    {
        currentState->Exit();
        newState->Enter();
        currentState = newState;
    }

}

void StateMachine::PushEvent(StateEvent event)
{
    currentState->events.append(event);
}

const QString& StateMachine::GetCurrentStateName() const
{
    return (currentState->stateName);
}
