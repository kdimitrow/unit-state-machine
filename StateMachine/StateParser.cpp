#include "StateParser.h"
#include <QDebug>
#include <QByteArray>


static const char START_STATE = '[';
static const char END_STATE = ']';
static const char START_EVENT = '<';
static const char END_EVENT = '|';
static const char START_CONDITION = '(';
static const char END_CONDITION = ')';
static const char START_LINK = '|';
static const char END_LINK = '>';
static const char START_COMMENT = '#';
static const char END_COMMENT = '\n';
static const char NEW_LINE = '\n';
static const char SPACE = ' ';
static const char TAB = '\t';

StateParser::StateParser()
{
}

StateParser::~StateParser()
{
}

bool StateParser::Parse(const ParserData& data)
{
    buffer = data.GetRawData();
    size = data.Size() - 1;
    oldState = kState;
    currentState = kState;

    for(int i = 0; i <= size; ++i)
    {
        oldState = currentState;
        currentState = WhatToRead(i);

        switch(currentState)
        {
        case kState:
            ++i;
            i += ReadState(i);
            break;
        case kEvent:
            ++i;
            i += ReadEvent(i);
            break;
        case kCondition:
            ++i;
            i += ReadCondition(i);
            break;
        case kLink:
            ++i;
            i += ReadLink(i);
            break;
        case kComment:
            ++i;
            i += ReadComment(i);
            break;

        case kParseError:
            qDebug() << "Error";
             qDebug() << i;
             qDebug() << buffer[i];
            return (false);
            break;

        default:
            // Do nothing
            break;
        }
    }

    return (true);
}

void StateParser::Serialize()
{

}

ReadingState StateParser::WhatToRead(int positionInBuffer)
{
    switch (buffer[positionInBuffer])
    {
    case START_STATE:
        return(kState);
        break;
    case START_EVENT:
        return(kEvent);
        break;
    case START_CONDITION:
        return(kCondition);
        break;
    case START_LINK:
        return(kLink);
        break;
    case START_COMMENT:
        return(kComment);
        break;
    case NEW_LINE:
        return(kPass);
    case SPACE:
        return(kPass);
    case TAB:
        return(kPass);
        break;
    default:
        return (kParseError);
        break;
    }
}

int StateParser::ReadState(int positionInBuffer)
{
    int consumed = 0;

    for(long long i = positionInBuffer; i <= size; ++i)
    {
        if(buffer[i] == END_STATE)
            break;
        else
            ++consumed;
    }


    QByteArray state(buffer + positionInBuffer,consumed);

    qDebug() <<"State: "<< state;


    return (consumed);
}

int StateParser::ReadEvent(int positionInBuffer)
{
    int consumed = 0;

    for(long long i = positionInBuffer; i <= size; ++i)
    {
        if(buffer[i] == END_EVENT)
            break;
        else
            ++consumed;
    }

    QByteArray event(buffer + positionInBuffer,consumed);

    qDebug() <<"Event: "<< event;


    return (consumed);

}

int StateParser::ReadCondition(int positionInBuffer)
{
    int consumed = 0;

    for(long long i = positionInBuffer; i <= size; ++i)
    {
        if(buffer[i] == END_CONDITION)
            break;
        else
            ++consumed;
    }

    QByteArray condition(buffer + positionInBuffer,consumed);

    qDebug() <<"Condition: "<< condition;


    return (consumed);

}

int StateParser::ReadLink(int positionInBuffer)
{
    int consumed = 0;

    for(long long i = positionInBuffer; i <= size; ++i)
    {
        if(buffer[i] == END_LINK)
            break;
        else
            ++consumed;
    }

    QByteArray link(buffer + positionInBuffer,consumed);

    qDebug() <<"Link: "<< link;


    return (consumed);

}

int StateParser::ReadComment(int positionInBuffer)
{
    int consumed = 0;

    for(long long i = positionInBuffer; i <= size; ++i)
    {
        if(buffer[i] == END_COMMENT)
            break;
        else
            ++consumed;
    }

    QByteArray comment(buffer + positionInBuffer,consumed);

    qDebug() <<"Comment: "<< comment;


    return (consumed);
}

