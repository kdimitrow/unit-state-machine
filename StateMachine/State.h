#ifndef STATE_H
#define STATE_H
#include<QList>
#include<QMap>
#include<QQueue>
#include<QString>
#include "StateEvent.h"
#include "EventComplement.h"

#define nullptr 0
#define UNUSED(expr) do { (void)(expr); } while (0)

class State;
typedef void (*EnterCallback)(State* thisState);
typedef bool (*ProcessCallback)(State* thisState);
typedef void (*ExitCallback)(State* thisState);
typedef QQueue<StateEvent> EventQueue;
typedef QMap<StateEvent::EventType,EventComplement> RegisteredEvents;
typedef QMap<StateEvent::EventType,EventComplement>::iterator RegisteredEventsIterator;

class State
{
public:
    State();
    ~State();

    void Enter();
    State* Process();
    void Exit();

    static void DummyEnter(State* thisState);
    static bool DummyProcess(State* thisState);
    static void DummyExit(State* thisState);

public:

    EnterCallback enter;
    ProcessCallback process;
    ExitCallback exit;

private:
public:
    EventQueue events;
    RegisteredEvents registeredEvents;
    QString stateName;
};

#endif // STATE_H
