#ifndef PARSERDATA_H
#define PARSERDATA_H

class ParserData
{
public:
    ParserData(const char* data = 0);
    ~ParserData();

    void Init(const char* data);
    void Clear();
    const char* GetRawData() const;
    long long Size() const;

private:
    char *buffer;
    long long size;
};

#endif // PARSERDATA_H
