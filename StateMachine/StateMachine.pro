#-------------------------------------------------
#
# Project created by QtCreator 2013-05-16T14:51:43
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = StateMachine
TEMPLATE = lib
CONFIG += staticlib

SOURCES += StateMachine.cpp

HEADERS += StateMachine.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

HEADERS += \
    State.h

SOURCES += \
    State.cpp

HEADERS += \
    StateEvent.h

SOURCES += \
    StateEvent.cpp

HEADERS += \
    StateConditions.h

SOURCES += \
    StateConditions.cpp

HEADERS += \
    EventComplement.h

HEADERS += \
    StateParser.h

SOURCES += \
    StateParser.cpp

HEADERS += \
    ParserData.h

SOURCES += \
    ParserData.cpp
